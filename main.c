#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "sphereview.h"

// Just use a global (eww!). I'd rather stash inside the GLFWwindow, but
// for a project this simple lazyness wins over.

static int active = 0;

static float rotx;
static float roty;

static float lastx;
static float lasty;

static float fov = 45.0f;


static void arcball_rotate(window_t *win) 
{ 
    matrixRotate(&win->view, roty, 1, 0, 0);
    matrixRotate(&win->view, rotx, 0, 1, 0);
} 

static void
mouse_pos(GLFWwindow *_win, double _x, double _y)
{ 
    assert(_win);
    float x = (float)_x;
    float y = (float)_y;

    float deltax = x - lastx;
    float deltay = y - lasty;

    if (active) {
        rotx += deltax;
        roty += deltay;
    }

    lastx = x;
    lasty = y;
} 

static void
mouse_button(GLFWwindow *win, int button, int action, int mods)
{
    assert(win);
    (void)(mods);

    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        if (action == GLFW_PRESS) {
            active = 1;
        } else {
            active = 0;
        }
    }
}



static void
mouse_scroll(GLFWwindow *_win, double x, double y)
{
    int w, h;
    glfwGetWindowSize(_win, &w, &h);

    window_t *win = glfwGetWindowUserPointer(_win);
    fov += (float)y * 0.1f;
    if (fov > 45) fov = 45;
    matrixPerspective(&win->projection,
            0.1f, 1000.0f,
            (float)w/(float)h,
            fov);

    (void)x;
}


int
main(int argc, char *argv[])
{
    printf("Creating window..\n");
    window_t *window = window_create(argc, argv);
    assert(window != NULL);
    assert(glGetError() == 0);

    printf("Creating texture..\n");
    texture_t *texture = texture_create(argc, argv);
    assert(texture != NULL);
    assert(glGetError() == 0);

    printf("Creating geometry..\n");
    geometry_t *geometry= geometry_create(argc, argv);
    assert(geometry!= NULL);
    assert(glGetError() == 0);

    printf("Creating shader..\n");
    shader_t *shader = shader_create(argc, argv);
    assert(shader != NULL);
    assert(glGetError() == 0);

    glfwSetMouseButtonCallback( window->handle, mouse_button);
    glfwSetCursorPosCallback(window->handle, mouse_pos);
    glfwSetScrollCallback(window->handle, mouse_scroll);

    printf("Ready!\n");

    while (!glfwWindowShouldClose (window->handle)) {
        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        assert(glGetError() == 0);

        glfwPollEvents();
        if (glfwGetKey(window->handle, GLFW_KEY_ESCAPE) == GLFW_PRESS) glfwSetWindowShouldClose(window->handle, 1);

        //matrixTranslate(&window->view, 0.0f, 0.0f, -20.0f);
        
        matrixIdentity(&window->view);
        arcball_rotate(window);


        glActiveTexture (GL_TEXTURE0);
        assert(glGetError() == 0);
        glBindTexture (GL_TEXTURE_2D, texture->handle);
        assert(glGetError() == 0);

        glUseProgram (shader->handle);
        assert(glGetError() == 0);
        glUniformMatrix4fv (shader->projection_loc,
                1, GL_FALSE,
                window->projection.f);
        assert(glGetError() == 0);

        glUniformMatrix4fv (shader->view_loc,
                1, GL_FALSE,
                window->view.f);
        assert(glGetError() == 0);

        glBindVertexArray(geometry->handle);
        assert(glGetError() == 0);
        glDrawElements(GL_TRIANGLES, geometry->num_indices, GL_UNSIGNED_INT, geometry->idx_offset);
        assert(glGetError() == 0);

        glfwSwapBuffers(window->handle);
        assert(glGetError() == 0);
    }

    glfwTerminate();
    return 0;
}

