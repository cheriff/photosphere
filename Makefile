
all: sphereview

run: sphereview
	./sphereview ./pano.jpg

OBJS=main.o matrix.o opengl.o shader.o texture.o geometry.o stb_image.o

LDFLAGS=-L/usr/local/lib/ -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -lglfw3
CCFLAGS=-march=core-avx2 -Wno-unused-macros -std=c11 -Weverything -Werror -Wno-unreachable-code -O2
DEBUG=-O0 -ggdb

sphereview: $(OBJS)
	@echo [LD]	sphereview
	@clang $(LDFLAGS) $(OBJS) -o sphereview


%.o: %.c
	@echo [CC]	$<
	@clang $(CCFLAGS) $(DEBUG) -DGLFW_INCLUDE_GLCOREARB=1 -c $< -o $@


# This external file isn't quite up to the pedantic warning/error levels used
# for the rest of our code. These warnings trigger, so for now we explicitly
# silence them. One day, ideally, I'll fix the code instead :)
STB_FLAGS=-Wno-sign-conversion -Wno-padded -Wno-cast-align -Wno-unused-macros -Wno-unreachable-code -Wno-undef -Wno-disabled-macro-expansion -Wno-shadow -Wno-unused-parameter -Wno-missing-field-initializers

stb_image.o: stb_image.c
	@echo [STB_CC]	$<
	@clang $(CCFLAGS) $(DEBUG) $(STB_FLAGS) -c $< -o $@

clean:
	rm -f sphereview
	rm -f $(OBJS)
