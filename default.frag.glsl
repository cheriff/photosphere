#version 400

in vec4 normal;
in vec2 tex;
out vec4 frag_colour;

const vec4 light = normalize(vec4(1,0,0,0));
const vec4 ambient = vec4(0.2, 0.2, 0.2, 1);

uniform sampler2D sampler;

void main () {
    frag_colour = texture(sampler, tex);
}
