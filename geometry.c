#include "sphereview.h"
#include "matrix.h"

#include <assert.h>
#include <stdlib.h>

/* Useful URLS:
 * https://github.com/mrdoob/three.js/blob/master/src/extras/geometries/SphereGeometry.js
 * https://code.google.com/p/away3d/source/browse/trunk/fp10/Away3DLite/src/away3dlite/primitives/Sphere.as?r=2888
 * http://gamedev.stackexchange.com/questions/31308/algorithm-for-creating-spheres
 * http://www.swiftless.com/tutorials/opengl/sphere.html
 * http://www.andrewnoske.com/wiki/Generating_a_sphere_as_a_3D_mesh
 * http://www.visualizationlibrary.org/documentation/_geometry_primitives_8cpp_source.html#l00284
 */

#define triangle 1
#define sphere 2
#define GEOM_TYPE sphere


typedef struct {
    vector_t position;
    vector_t normal;
    vector_t tex;
} vertex_t;

typedef struct {
    vertex_t *vertices;
    uint32_t *indices;

    uint32_t num_vertices;
    uint32_t num_indices;
} geom_data_t;


// Pull in the appropriate geometry builder in a header.
// Mainly to keep separation of code for different geom types.
#if (GEOM_TYPE == triangle)
#include "triangle_geometry.h"
#elif (GEOM_TYPE == sphere)
#include "sphere_geometry.h"
#endif


static geom_data_t*
get_geometry(void)
{
#if GEOM_TYPE == triangle
    return get_triangle_geometry();
#elif GEOM_TYPE == sphere
    return get_sphere_geometry();
#else
    #error Unknown geometry
#endif
}



geometry_t *
geometry_create(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    // Calculate our sizes and offsets:
    uint32_t vertex_size = sizeof(vertex_t);
    uint32_t index_size = sizeof(uint32_t);

    // factor out actual generation of the vertices and indices for ease of
    // swapping for something else out later
    geom_data_t *g = get_geometry();

    uint32_t num_verts = g->num_vertices;
    uint32_t num_indices = g->num_indices;

    // Vertices will go at the beginning of the buffer
    uint32_t vertex_offset = 0;

    // And We'll place the indices right after the vertex data in the buffer.
    uint32_t index_offset = num_verts * vertex_size;

    uint32_t buffer_size = (num_verts * vertex_size) + (num_indices * index_size);

    geometry_t *ret = malloc(sizeof(*ret));

    /* First off, create the VAO to capture bindings, pointer types/offsets for
     * ths model
     */
    glGenVertexArrays (1, &ret->handle);
    glBindVertexArray (ret->handle);

    // Generate one buffer for the model
    // We'll allocate enough size for vertex and index data, but not yet load with data
    glGenBuffers (1, &ret->bo);
    glBindBuffer (GL_ARRAY_BUFFER, ret->bo);
    glBufferData (GL_ARRAY_BUFFER, buffer_size, NULL, GL_STATIC_DRAW);

    // Now load in the data, noting that indices follow immediately after the
    // vertices
    glBufferSubData(GL_ARRAY_BUFFER, (GLintptr)vertex_offset, vertex_size*num_verts, g->vertices);
    glBufferSubData(GL_ARRAY_BUFFER, (GLintptr)index_offset, index_size*num_indices, g->indices);

    // In order to use the indices portion of the buffer, we must bind it as an
    // element array too.
    // It seems OpenGL is happy with one buffer bound twice, we just have to
    // note the offset from the buffer to the indices. (there's nothing similar
    // to 'glVertexAttribPointer' for index data)
    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, ret->bo);

    // Finally, describe the vertex format. Use a throwaway NULL instantiation of
    // vertex_t to calculate sizes and offsets.
    vertex_t *v = NULL;
    glEnableVertexAttribArray (0);
    glEnableVertexAttribArray (1);
    glEnableVertexAttribArray (2);
    glVertexAttribPointer (0, 4, GL_FLOAT, GL_FALSE, sizeof(*v), &v->position);
    glVertexAttribPointer (1, 4, GL_FLOAT, GL_TRUE, sizeof(*v), &v->normal); // we DO want normals normalised
    glVertexAttribPointer (2, 2, GL_FLOAT, GL_FALSE, sizeof(*v), &v->tex);

    // Stash the number of intices to draw
    ret->num_indices = (GLsizei)num_indices;
    // And note the offset from the buffer base where they are
    ret->idx_offset = (void*)(uintptr_t)index_offset;

    return ret;
}
