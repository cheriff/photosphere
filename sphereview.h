#include <GLFW/glfw3.h>

#include <stdint.h>

#include "matrix.h"


typedef struct {
    GLFWwindow *handle;
    matrix_t projection;
    matrix_t view;
} window_t;
window_t *window_create(int argc, char *argv[]);


typedef struct {
    uint32_t handle;
} texture_t;

texture_t *texture_create(int argc, char *argv[]);

typedef struct {
    uint32_t handle;
    uint32_t bo;
    void* idx_offset;
    GLsizei num_indices;
    uint32_t pad0;
} geometry_t;

geometry_t *geometry_create(int argc, char *argv[]);

typedef struct {
    uint32_t handle;
    GLint projection_loc;
    GLint view_loc;
} shader_t;

shader_t *shader_create(int argc, char *argv[]);
