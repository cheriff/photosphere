
static vertex_t tri_vertices[] = {
    {           //Top middle
        .position = {{ 0.0f,  0.5f,  0.0f, 1.0f, }},
        .normal   = {{ 0.0f,  0.0f,  1.0f, 0.0f, }},
        .tex      = {{ 0.5, -0.5f }},
    },
    {           //bottom right
        .position = {{ 0.5f, -0.5f,  0.0f, 1.0f, }},
        .normal   = {{ 0.0f,  0.0f,  1.0f, 0.0f, }},
        .tex      = {{ 1, 1 }},
    },
    {           //bottom left
        .position = {{ -0.5f, -0.5f,  0.0f, 1.0f, }},
        .normal   = {{ 0.0f,  0.0f,  1.0f, 0.0f, }},
        .tex      = {{ 0, 1 }},
    },
};
static uint32_t tri_indices[] = { 0, 1, 2 };

static geom_data_t*
get_triangle_geometry(void)
{
    static geom_data_t ret = {
        .vertices       = tri_vertices,
        .num_vertices   = sizeof(tri_vertices)/sizeof(*tri_vertices),
        .indices        = tri_indices,
        .num_indices    = sizeof(tri_indices)/sizeof(*tri_indices)
    };
    return &ret;
}
