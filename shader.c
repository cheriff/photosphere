#include "sphereview.h"
#include <assert.h>
#include <string.h>
#include <stdlib.h>

static unsigned int program_from_shaders(unsigned int vertex_shader, unsigned int fragment_shader);
static unsigned int shader_from_file(const char *fname, GLenum shaderType);
static void dump_program(unsigned int p);


shader_t *
shader_create(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    char * vert_fname = "default.vert.glsl";
    char * frag_fname = "default.frag.glsl";


    unsigned int vert_shader = shader_from_file(vert_fname, GL_VERTEX_SHADER);
    if (vert_shader == 0) return NULL;

    unsigned int fragment_shader = shader_from_file(frag_fname, GL_FRAGMENT_SHADER);
    if (fragment_shader == 0) {
        glDeleteShader(vert_shader);
        return NULL;
    }

    unsigned int program = program_from_shaders(vert_shader, fragment_shader);
    if (!program) {
        glDeleteShader(vert_shader);
        glDeleteShader(fragment_shader);
        return NULL;
    }

    if (0) dump_program(program);

    shader_t *ret = malloc(sizeof(*ret));

    ret->handle = program;
    ret->view_loc = glGetUniformLocation (program, "view");
    ret->projection_loc = glGetUniformLocation (program, "projection");
    assert(ret->view_loc != -1);
    assert(ret->projection_loc != -1);

    return ret;
}

static char *
glType_to_string(GLenum type)
{
    switch(type) {
#define ITEM(name, num) case num: return #name;
    ITEM(GL_FLOAT_VEC2       , 0x8B50)
    ITEM(GL_FLOAT_VEC3       , 0x8B51)
    ITEM(GL_FLOAT_VEC4       , 0x8B52)
    ITEM(GL_INT_VEC2         , 0x8B53)
    ITEM(GL_INT_VEC3         , 0x8B54)
    ITEM(GL_INT_VEC4         , 0x8B55)
    ITEM(GL_BOOL             , 0x8B56)
    ITEM(GL_BOOL_VEC2        , 0x8B57)
    ITEM(GL_BOOL_VEC3        , 0x8B58)
    ITEM(GL_BOOL_VEC4        , 0x8B59)
    ITEM(GL_FLOAT_MAT2       , 0x8B5A)
    ITEM(GL_FLOAT_MAT3       , 0x8B5B)
    ITEM(GL_FLOAT_MAT4       , 0x8B5C)
    ITEM(GL_SAMPLER_1D       , 0x8B5D)
    ITEM(GL_SAMPLER_2D       , 0x8B5E)
    ITEM(GL_SAMPLER_3D       , 0x8B5F)
    ITEM(GL_SAMPLER_CUBE     , 0x8B60)
    ITEM(GL_SAMPLER_1D_SHADOW, 0x8B61)
    ITEM(GL_SAMPLER_2D_SHADOW, 0x8B62)
#undef ITEM
        default:
            printf("Unknown: %04x\n", type);
            assert(!"Fail");
    }
}

static void
dump_program(unsigned int program)
{
    int attributes;
    int uniforms;
    static char item_name[1024] = {0};
    unsigned int type;
    int size;

    glGetProgramiv (program, GL_ACTIVE_ATTRIBUTES, &attributes);
    for (int i=0; i<attributes; i++) {
        printf("Attribute %d\n", i);
        glGetActiveAttrib(program, (GLuint)i, 1024, NULL, &size, &type, item_name);
        printf("\tName: %s\n", item_name);
        printf("\tType: %x / %s\n", type, glType_to_string(type));
        printf("\tSize: %d\n", size);
        int location = glGetAttribLocation (program, item_name);
        printf("\tLocation = %d\n", location);
    }

    glGetProgramiv (program, GL_ACTIVE_UNIFORMS, &uniforms);
    for (int i=0; i<uniforms; i++) {
        printf("Uniform %d\n", i);
        glGetActiveUniform (program, (GLuint)i, 1024, NULL, &size, &type, item_name);
        printf("\tName: %s\n", item_name);
        printf("\tType: %x / %s\n", type, glType_to_string(type));
        printf("\tSize: %d\n", size);
        int location = glGetUniformLocation (program, item_name);
        printf("\tLocation = %d\n", location);
    }
}

static unsigned int
program_from_shaders(unsigned int vertex_shader, unsigned int fragment_shader)
{
    assert(vertex_shader);
    assert(fragment_shader);

     unsigned int program = glCreateProgram ();
     glAttachShader (program, vertex_shader);
     glAttachShader (program, fragment_shader);

     glBindAttribLocation (program, 1, "vertex_position");

     glLinkProgram (program);

     int link_status;
     glGetProgramiv(program, GL_LINK_STATUS, &link_status);
     if (!link_status) {
         int log_len;
         glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_len);
         char *log = alloca(log_len);

         glGetProgramInfoLog(program, log_len, NULL, log);
         printf("Link FAIL:\n%s\n", log);
         glDeleteProgram(program);
         program = 0;
     }

     return program;
}

static size_t
util_fileSize(const char *path)
{
    assert(path);

    FILE *fp = fopen(path, "r");
    if (!fp) {
        printf("NO SUCH FILE: %s\n", path);
        return 0;
    }

    int result = fseek(fp, 0, SEEK_END);
    if (result) {
        perror("fseek");
        assert(result == 0);
    }

    long size = ftell(fp);
    if (size < 0) {
        perror("fseek");
        assert(size >= 0);
    }

    fclose(fp);

    return (size_t)size;
}

static unsigned int
shader_from_file(const char *fname, GLenum shaderType)
{
    size_t len = util_fileSize(fname);
    if (len == 0)  return 0;

    FILE *fp = fopen(fname, "r");
    if (!fp) {
        perror("fopen");
        return 0;
    }
    char * src = malloc(len);
    size_t got = fread(src, 1, len, fp);
    assert(got == len);
    fclose(fp);

    const char * sources[] = { src };
    GLint sizes[] = { (GLint)len };

    unsigned int shader = glCreateShader(shaderType);
    assert(shader != 0);

    glShaderSource (shader, 1, sources, sizes);
    glCompileShader(shader);

    int compiled_ok;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled_ok);
    if (!compiled_ok)  {
        int log_len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_len);

        char *log = alloca(log_len);
        glGetShaderInfoLog(shader, log_len, NULL, log);
        char *type = shaderType == GL_VERTEX_SHADER? "Vertex" : "Fragment";
        printf("COMPILE FAIL: %s\n%s\n\n", type, log);
        glDeleteShader(shader);
        shader = 0;
    }

    return shader;

}
