#include "sphereview.h"
#include <assert.h>
#include <stdlib.h>

#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

static void 
errorHappened(int code, const char* str)
{
    printf("GLFW Error(%d) :: %s\n", code, str);
}

static void
windowMoved(GLFWwindow *win, int x, int y)
{
    printf("MOVE: Window %p:  %d %d\n", win, x, y);
}

static void
windowResized(GLFWwindow *win, int x, int y)
{
    float sizeX = (float)x;
    float sizeY = (float)y;

    window_t *w = glfwGetWindowUserPointer(win);
    matrixPerspective(&w->projection,
            0.1f, 1000.0f,
            sizeX/sizeY,
            45.0);
}

window_t *
window_create(int argc, char *argv[])
{
    // Maybe one day we'll use commandline arguments to specify things
    // like fullscreen/windowed, resolution, etc.
    (void)argc;
    (void)argv;

    if (glfwInit() != GL_TRUE) {
        return NULL;
    }

    //Request Specific Version
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow *window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT,
            argv[0], NULL, NULL);
    if (!window) {
        return NULL;
    }

    window_t *ret = malloc(sizeof(*ret));

    ret->handle = window;
    glfwSetWindowUserPointer(window, ret);

    matrixIdentity(&ret->projection);
    matrixIdentity(&ret->view);

    glfwSetWindowPosCallback(window, windowMoved);
    glfwSetWindowSizeCallback(window, windowResized);
    glfwSetErrorCallback(errorHappened);
    glfwMakeContextCurrent(window);

     glfwSwapInterval(1);

     glEnable (GL_DEPTH_TEST);
     glDepthFunc (GL_LESS);

     // GLFW won't send us an initial 'resized' event with the initial
     // width and height, so we'll force one now.
     windowResized(window, WINDOW_WIDTH, WINDOW_HEIGHT);

     return ret;
}
