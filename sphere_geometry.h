/* Sphere builder.
 * Port of:
 *    https://github.com/mrdoob/three.js/blob/master/src/extras/geometries/SphereGeometry.js
 */

#include <math.h>

static geom_data_t*
get_sphere_geometry()
{
    const float radius = 10.0;
    const float arcLength = 1.0;
    const int segmentsW = 60;
    const int segmentsH = 80;

    int i, j;
    int minj = (int)(segmentsH * (1-arcLength));


    // Calculate how many indices and triangles we're about to generate
    int num_tris = 2 * segmentsW + ((segmentsH-2) * segmentsW * 2);

    geom_data_t *ret = malloc(sizeof(*ret));
    ret->num_vertices = (uint32_t)(segmentsH-minj+1) * (segmentsW+1);
    ret->num_indices = (uint32_t)num_tris * 3;

    ret->vertices = malloc(ret->num_vertices * sizeof(vertex_t));
    ret->indices = malloc(ret->num_indices * sizeof(uint32_t));

    int num_verts =0;
    for (j=minj; j<= segmentsH; j++) {
        float horangle = (float)M_PI*j/segmentsH;
        float z = radius * cosf(horangle);
        float ringradius = radius*sinf(horangle);

        for (i = 0; i <= segmentsW; i++) {
            float verangle = 2*(float)M_PI*i/segmentsW;

            float x = ringradius*cosf(verangle);
            float y = ringradius*sinf(verangle);

            //vertices.push(x, -z, y)
            ret->vertices[num_verts].position.f[0] = x;
            ret->vertices[num_verts].position.f[1] = z;
            ret->vertices[num_verts].position.f[2] = y;
            ret->vertices[num_verts].position.f[3] = 1.0f;

            //_uvtData.push(i/_segmentsW, 1 - (j - minj)/(_segmentsH - minj), 1);
            ret->vertices[num_verts].tex.f[0] = (float)(i)/(float)(segmentsW);
            ret->vertices[num_verts].tex.f[1] = (float)(j - minj)/(float)(segmentsH - minj);

            // Normals of a sphere are just the position, normalised
            ret->vertices[num_verts].normal.f[0] = x/radius;
            ret->vertices[num_verts].normal.f[1] = -z/radius;
            ret->vertices[num_verts].normal.f[2] = y/radius;
            ret->vertices[num_verts].normal.f[3] = 0.0f;

            num_verts++;
        }
    }

    int num_indices = 0;
    for (j = 1; j <= segmentsH - minj; ++j) {
        for (i = 1; i <= segmentsW; ++i) {
            uint32_t a = (uint32_t)((segmentsW + 1)*j + i);
            uint32_t b = (uint32_t)((segmentsW + 1)*j + i - 1);
            uint32_t c = (uint32_t)((segmentsW + 1)*(j - 1) + i - 1);
            uint32_t d = (uint32_t)((segmentsW + 1)*(j - 1) + i);

            if (j == (segmentsH - minj)) {
                // Triangle
                ret->indices[num_indices+0] = a;
                ret->indices[num_indices+1] = c;
                ret->indices[num_indices+2] = d;
                num_indices += 3;

            } else if (j == (1 - minj)) {
                // Triangle
                ret->indices[num_indices+0] = a;
                ret->indices[num_indices+1] = b;
                ret->indices[num_indices+2] = c;
                num_indices += 3;

            } else {
                // Quad - aka two triangles
                ret->indices[num_indices+0] = a;
                ret->indices[num_indices+1] = b;
                ret->indices[num_indices+2] = c;

                ret->indices[num_indices+3] = a;
                ret->indices[num_indices+4] = c;
                ret->indices[num_indices+5] = d;
                num_indices += 6;
            }
        }
    }

    // Double check we did the pre-calcs rights
    assert((uint32_t)num_indices == ret->num_indices);
    assert((uint32_t)num_verts == ret->num_vertices);
    printf("Created sphere with %d triangles over %d vertices\n",
            num_indices, num_tris);

    return ret;

}
