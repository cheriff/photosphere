#version 400

layout(location = 0 ) in vec4 vertex_position;
layout(location = 1 ) in vec4 vertex_normal;
layout(location = 2 ) in vec2 texture_coordinate;
uniform mat4 view, projection;

out vec4 normal;
out vec2 tex;

void main () {

    gl_Position = projection * view * vertex_position, 1.0;

    mat4 rot = mat4(view[0], view[1], view[2], vec4(0,0,0,1));
    normal = normalize(transpose(inverse(rot))*vertex_normal);
    tex = texture_coordinate;
}
