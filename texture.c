#include "sphereview.h"
#include <assert.h>
#include <stdlib.h>

#include "stb_image.h"

texture_t *
texture_create(int argc, char *argv[])
{
    if (argc < 2) {
        printf("Usage: %s <filename>\n", argv[0]);
        printf("\tWhere <filename> is a longlat photosphere image\n");
        return NULL;
    }


    int width, height, depth;
    void *pixels = stbi_load(argv[1], &width, &height, &depth, 4);
    if (!pixels) {
        printf("Unable to load %s : %s\n", argv[1], stbi_failure_reason());
        return NULL;
    }

    texture_t *ret = malloc(sizeof(*ret));
    glGenTextures(1, &ret->handle);

    glActiveTexture (GL_TEXTURE0);
    glBindTexture (GL_TEXTURE_2D, ret->handle);

    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA,
            width, height,
            0, GL_RGBA, GL_UNSIGNED_BYTE,
            pixels);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    return ret;
}
